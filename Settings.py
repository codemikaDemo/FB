import pygame
pygame.init()

# Display / window
width, height = 1200, 900
background = pygame.Color('blue')

# Bird
bird_img = pygame.image.load('bird.png')
bird_speed = 2
bird_speed_fly = 0.5
bird_speed_jump = 3
bird_width = bird_img.get_width()
bird_height = bird_img.get_height()

# Wall
wall_x = width
wall_y = height * 1 / 2
wall2_x = width * 1.5
wall2_y = - height * 1 / 4
wall_img = pygame.image.load('wall.png')
wall_width = wall_img.get_width()
wall_height = wall_img.get_height()
wall_speed = 1
wall_speed_turbo = 5

# Cheats
invisible = False # Птичка не сталкивается с препятствиями, если нажать на клавишу m
turbo = False