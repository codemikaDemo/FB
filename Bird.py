from Settings import *

class Bird: # Создание класса Птичка
    x, y = 0, 0
    img = bird_img
    speed = bird_speed
    live = 3

    def __init__(self):
        self.center()

    def check(self, x, y, w, h):
        return (0 <= self.x <= width and self.y > height) \
            or (x < self.x < x + w and y < self.y < y + h)\
            or (x < self.x + bird_width < x + w and y < self.y < y + h)\
            or (x < self.x < x + w and y < self.y + bird_height < y + h)\
            or (x < self.x + bird_width < x + w and y < self.y + bird_height < y + h)


    def right(self): # Функция Лететь вперед
        self.x += self.speed
        if self.x > width:
            self.x = -bird_width

    def left(self): # Функция Лететь назад
        self.x -= self.speed
        if self.x < -bird_width:
            self.x = width

    def fly(self):
        self.y += bird_speed_fly

    def jump(self):
        if self.y > 0:
            self.y -= bird_speed_jump

    def hide(self):
        self.x = width * 2

    def center(self):
        self.x, self.y = width / 2, height / 2