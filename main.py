from Settings import *
from Bird import *

pygame.init() # Запуск библиотеки
window = pygame.display.set_mode((width, height)) # Создаем окно программы, размером 500*500 пикселей
bird = Bird()
bally = 0
font = pygame.font.SysFont('serif', 50)
while True: # Запускаем беконечный цикл
    for e in pygame.event.get(): # Проверяем все события, проиходящие с нашим окном
        if e.type == pygame.QUIT: # Если на окне была нажата кнопка с крестиком, то 
            exit() # Программа завершается

    # Основной код программы
    key = pygame.key.get_pressed()
    if key[pygame.K_RIGHT]: bird.right()
    if key[pygame.K_LEFT]: bird.left()
    if key[pygame.K_SPACE]: bird.jump()
    if key[pygame.K_h]: bird.hide()
    if key[pygame.K_c]: bird.center()
    if key[pygame.K_m]: invisible = True
    if key[pygame.K_n]: invisible = False
    if key[pygame.K_t]: turbo = True
    if key[pygame.K_y]: turbo = False

    bird.fly()

    if not invisible and bird.check(wall_x, wall_y, wall_width, wall_height):
        bally = 0
        bird.center()
        wall_x = width
        bird.live -= 1
    if bird.live == 0:
        exit()
    if turbo:
        wall_x -= wall_speed_turbo
    else:
        wall_x -= wall_speed
        wall2_x -= wall_speed
    if wall_x < wall_width:
        wall_x = width
    if wall2_x < wall_width:
        wall2_x = width

    bally += 0.001
    text_bally = font.render('Баллы: ' + str(int(bally)), True, pygame.Color('white'))
    text_live = font.render('Жизни: ' + str(bird.live), True, pygame.Color('white'))
    text_cheats = font.render('Чит неведимка: on', True, pygame.Color('white'))
    text_turbo = font.render('Турбо режим: on', True, pygame.Color('white'))

    window.fill(background) # Закрашиваем окно в синий цвет
    window.blit(bird.img, (bird.x, bird.y))
    window.blit(wall_img, (wall_x, wall_y))
    window.blit(wall_img, (wall2_x, wall2_y))
    window.blit(text_bally, (20, 20))
    window.blit(text_live, (20, 60))
    if invisible:
        window.blit(text_cheats, (20, 100))
    if turbo:
        window.blit(text_turbo, (20, 140))
    pygame.display.update() # Обновляем содержимое окна
pygame.quit()